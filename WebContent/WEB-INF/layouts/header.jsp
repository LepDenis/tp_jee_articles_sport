<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header>
	<nav>
		<ul>
			<li><a href="${ pageContext.request.contextPath }/home">Home</a></li>
			<li><a href="${ pageContext.request.contextPath }/articles">Articles</a></li>
			<li><a href="${ pageContext.request.contextPath }/inscription">Inscription</a></li>
			<c:choose>
				<c:when test="${ not empty sessionScope.user }">
					<li><a href="${ pageContext.request.contextPath }/logout">Se
							déconnecter</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="${ pageContext.request.contextPath }/login">Se
							connecter</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	</nav>
</header>