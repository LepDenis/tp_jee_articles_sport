<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="bienvenue">
<br><br><br>
	<c:choose>
		<c:when test="${ not empty sessionScope.user }">
			<h2>Bienvenue, ${ sessionScope.user.firstName } !</h2>
		</c:when>
		<c:otherwise>
			<h2>Bienvenue, invité !</h2>
		</c:otherwise>
	</c:choose>
</div>

<section id="video">
	<figure>
		<video
			src="${ pageContext.request.contextPath }/assets/videos/Nike_Joyride_Behind_the_Design_Nike.mp4"
			muted autoplay loop></video>
	</figure>
</section>