<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<section id="form">
	<form action="${ pageContext.request.contextPath }/inscription" method="post">
		<div>
			<label for="name">Nom</label> <input type="text" id="name"
				name="lastname" placeholder="Nom">
		</div>
		<div>
			<label for="prenom">Prénom</label> <input type="text" id="prenom"
				name="firstname" placeholder="Prenom">
		</div>
		<div>
			<label for="email">Email</label> <input type="email" id="email"
				name="email" placeholder="Email">
		</div>
		<div>
			<label for="pwd">Mot de passe</label> <input type="password" id="pwd"
				name="pwd" placeholder="Mot de passe">
		</div>
		<div>
			<button type="submit">Valider</button>
		</div>
	</form>
</section>