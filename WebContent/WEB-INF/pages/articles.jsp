<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<section id="articles">

	<c:forEach var="article" items="${ articles }">

		<article>
			<figure>
				<img
					src="${ pageContext.request.contextPath }/assets/images/${ article.imgLink }"
					alt="">
				<figcaption>
					<h4>${ article.name }</h4>
					<p>${ article.shortDesc }</p>
				</figcaption>
			</figure>
		</article>
		
	</c:forEach>
</section>