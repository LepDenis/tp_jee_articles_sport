<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<section id="login">

	<c:if test="${ not empty error }">
		<div>
			<h4 style="color: red;">${ error }</h4>
		</div>
	</c:if>

	<form method="post" action="${ pageContext.request.contextPath }/login">
		<div>
			<label for="a">E-Mail : </label>
			<div>
				<input type="email" id="a"
					placeholder="Adresse E-Mail..." name="email" />
			</div>
		</div>
		<br>
		<div>
			<label for="b">Mot de Passe : </label>
			<div>
				<input type="password" id="b"
					placeholder="Mot de Passe..." name="mdp" />
			</div>
		</div>
		<br>
		<div>
			<button type="submit">Se connecter</button>
		</div>
	</form>

</section>