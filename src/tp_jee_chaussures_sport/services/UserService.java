package tp_jee_chaussures_sport.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

import tp_jee_chaussures_sport.models.User;

public class UserService {

	private File file = new File("users.txt");

	public void inscrire(User user) {

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {

			String str = user.getEmail() + "," + user.getLastName() + "," + user.getFirstName() + ","
					+ user.getPassword() + "\n";
			writer.write(str);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Optional<User> find(String email) {

		Optional<User> user = Optional.empty();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			Optional<String> infos = reader.lines().filter(line -> line.startsWith(email)).findAny();
			if (infos.isPresent()) {
				String[] splitLine = infos.get().split(",");
				user = Optional.of(new User(splitLine[1], splitLine[2], splitLine[0], splitLine[3]));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return user;
	}
}
