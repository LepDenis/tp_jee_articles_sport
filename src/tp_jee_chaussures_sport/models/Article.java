package tp_jee_chaussures_sport.models;

public class Article {

	private String name;
	private String imgLink;
	private String shortDesc;
	private String longDesc;

	public Article(String name, String imgLink, String shortDesc, String longDesc) {
		this.name = name;
		this.imgLink = imgLink;
		this.shortDesc = shortDesc;
		this.longDesc = longDesc;
	}

	public Article() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

}
