package tp_jee_chaussures_sport.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tp_jee_chaussures_sport.models.User;
import tp_jee_chaussures_sport.services.UserService;

@WebServlet("/inscription")
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("template", "inscription");
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = new User(request.getParameter("lastname"), request.getParameter("firstname"),
				request.getParameter("email"), request.getParameter("pwd"));

		UserService serv = new UserService();
		serv.inscrire(user);
		request.getSession().setAttribute("user", user);
		response.sendRedirect(request.getContextPath() + "/");
	}

}
