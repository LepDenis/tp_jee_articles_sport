package tp_jee_chaussures_sport.servlets;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tp_jee_chaussures_sport.models.User;
import tp_jee_chaussures_sport.services.UserService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("template", "login");
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String email = request.getParameter("email");
		String mdp = request.getParameter("mdp");
		UserService service = new UserService();
		Optional<User> user = service.find(email);

		if (user.isPresent()) {
			if (user.get().getPassword().equals(mdp)) {
				request.getSession().setAttribute("user", user.get());
				response.sendRedirect(request.getContextPath() + "/");
			} else {
				request.setAttribute("error", "Mot de Passe incorrect !!");
				doGet(request, response);
			}
		} else {
			request.setAttribute("error", "E-Mail incorrect !!");
			doGet(request, response);
		}
	}
}
