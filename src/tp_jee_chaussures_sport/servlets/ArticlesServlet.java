package tp_jee_chaussures_sport.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tp_jee_chaussures_sport.models.Article;

@WebServlet("/articles")
public class ArticlesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Article> articles = new ArrayList<>(Arrays.asList(new Article("Nike Air Max 2090 BETRUE",
				"chaussure-air-max-2090-betrue-pour-kWPmsk.jpg", "Chaussure pour homme",
				"Praesent bibendum lacinia dolor, et semper lectus faucibus quis. Aliquam id mattis leo, sit amet molestie ante. Nulla facilisi. Morbi vulputate tincidunt augue, nec scelerisque orci mollis ut. Nunc sed felis bibendum, fringilla diam lobortis, finibus nulla. Maecenas ultrices lorem eu pharetra finibus. Ut sed turpis tellus."),

				new Article("Nike Air Force 1 BETRUE", "chaussure-air-force-1-betrue-pour-fbbFZg.jpg",
						"Chaussure pour homme",
						"Praesent bibendum lacinia dolor, et semper lectus faucibus quis. Aliquam id mattis leo, sit amet molestie ante. Nulla facilisi. Morbi vulputate tincidunt augue, nec scelerisque orci mollis ut. Nunc sed felis bibendum, fringilla diam lobortis, finibus nulla. Maecenas ultrices lorem eu pharetra finibus. Ut sed turpis tellus."),

				new Article("Nike BETRUE", "sweat-a-capuche-betrue-1JmLXF.jpg", "Sweat à capuche",
						"Praesent bibendum lacinia dolor, et semper lectus faucibus quis. Aliquam id mattis leo, sit amet molestie ante. Nulla facilisi. Morbi vulputate tincidunt augue, nec scelerisque orci mollis ut. Nunc sed felis bibendum, fringilla diam lobortis, finibus nulla. Maecenas ultrices lorem eu pharetra finibus. Ut sed turpis tellus."),

				new Article("Nike Sportswear BETRUE", "tee-shirt-sportswear-betrue-TrFvrh.jpg", "Tee-shirt",
						"Praesent bibendum lacinia dolor, et semper lectus faucibus quis. Aliquam id mattis leo, sit amet molestie ante. Nulla facilisi. Morbi vulputate tincidunt augue, nec scelerisque orci mollis ut. Nunc sed felis bibendum, fringilla diam lobortis, finibus nulla. Maecenas ultrices lorem eu pharetra finibus. Ut sed turpis tellus."),

				new Article("Nike Sportswear BETRUE", "tee-shirt-sportswear-betrue-pour-fSGPT8.jpg",
						"Tee-shirt pour homme",
						"Praesent bibendum lacinia dolor, et semper lectus faucibus quis. Aliquam id mattis leo, sit amet molestie ante. Nulla facilisi. Morbi vulputate tincidunt augue, nec scelerisque orci mollis ut. Nunc sed felis bibendum, fringilla diam lobortis, finibus nulla. Maecenas ultrices lorem eu pharetra finibus. Ut sed turpis tellus."),

				new Article("Nike Sportswear BETRUE", "tee-shirt-court-sportswear-betrue-pour-qzLRL8.jpg",
						"Tee-shirt pour femme",
						"Praesent bibendum lacinia dolor, et semper lectus faucibus quis. Aliquam id mattis leo, sit amet molestie ante. Nulla facilisi. Morbi vulputate tincidunt augue, nec scelerisque orci mollis ut. Nunc sed felis bibendum, fringilla diam lobortis, finibus nulla. Maecenas ultrices lorem eu pharetra finibus. Ut sed turpis tellus.")));

		request.setAttribute("articles", articles);
		request.setAttribute("template", "articles");
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
